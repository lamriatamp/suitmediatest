<?php


$apiUrl = 'https://suitmedia-backend.suitdev.com/api/ideas';


$params = [
    'page[number]' => $_GET['page_number'] ?? 1,
    'page[size]' => $_GET['page_size'] ?? 10,
    'append' => ['small_image', 'medium_image'],
    'sort' => $_GET['sort'] ?? 'published_at',
];

$apiUrl .= '?' . http_build_query($params);

$headers = [
    'Content-Type: application/json',
    'Accept: application/json', 
];

$ch = curl_init($apiUrl);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$response = curl_exec($ch);

if (curl_errno($ch)) {
    die('Error: ' . curl_error($ch));
}

// Close cURL session
curl_close($ch);

// Output the API response
header('Content-Type: application/json');
echo $response;
?>
