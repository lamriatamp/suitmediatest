document.addEventListener('DOMContentLoaded', function () {
    const postContainer = document.getElementById('postContainer');
    const paginationContainer = document.getElementById('pagination');
    let postsData = [];
    let currentPage = 1;
    let itemsPerPage = 10;

    // Memuat preferensi pengguna dari localStorage
    const savedPage = localStorage.getItem('currentPage');
    const savedItemsPerPage = localStorage.getItem('itemsPerPage');

    if (savedPage) {
        currentPage = parseInt(savedPage, 10);
    }

    if (savedItemsPerPage) {
        itemsPerPage = parseInt(savedItemsPerPage, 10);
    }

    function renderPosts(posts) {
        postContainer.innerHTML = '';
        posts.forEach(post => {
        });
    }

    function renderPagination(totalPages) {
        paginationContainer.innerHTML = '';
        for (let i = 1; i <= totalPages; i++) {
            const button = document.createElement('button');
            button.textContent = i;
            button.addEventListener('click', () => {
                showPerPage(i);
            });
            paginationContainer.appendChild(button);
        }
    }

    function sortPostsByDate(order) {
        showPerPage(currentPage); 
    }

    function showPerPage(pageNum) {
        currentPage = pageNum;
        localStorage.setItem('currentPage', currentPage); 

        const startIndex = (pageNum - 1) * itemsPerPage;
        const endIndex = startIndex + itemsPerPage;
        const visiblePosts = postsData.slice(startIndex, endIndex);

        renderPosts(visiblePosts);
    }

    function updateItemsPerPage(size) {
        itemsPerPage = size;
        localStorage.setItem('itemsPerPage', itemsPerPage); 
        showPerPage(currentPage); 
    }

    
    fetch('https://suitmedia-backend.suitdev.com/api/ideas', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then(response => response.json())
        .then(data => {
            postsData = data;
            sortPostsByDate('desc'); // Pengurutan awal
            renderPagination(Math.ceil(postsData.length / itemsPerPage));
            showPerPage(currentPage);
        })
        .catch(error => console.error('Error fetching data:', error));
});
